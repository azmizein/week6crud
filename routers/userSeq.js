const router = require("express").Router();
const { userSeq } = require("../controllers/index");
const { multerUpload } = require("../helper/multer.js");

router.post("/register", userSeq.register);
router.post("/login", userSeq.login);
router.get("/getAll", userSeq.findAllUser);
router.get("/getById/:id", userSeq.findById);
router.put("/update/:id", userSeq.update);
router.delete("/remove/:id", userSeq.remove);
router.post("/changeotp", userSeq.changeOtp);
router.post("/verification", userSeq.verification);
router.put("/changePassword/:id", userSeq.changePassword);
router.post(
  "/single-uploaded/:id",
  multerUpload.single("file"),
  userSeq.uploadFile
);

module.exports = router;
