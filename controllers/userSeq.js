const db = require("../models");
const user = db.User;
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const fs = require("fs");
const handlebars = require("handlebars");
const transporter = require("../helper/transporter");

module.exports = {
  register: async (req, res) => {
    try {
      const { username, password, email, confirmPassword } = req.body;

      if (!username || !email || !password) {
        return res.status(400).send({
          message: "Form cannot be null",
        });
      }

      const emailValidation = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
      if (!emailValidation.test(email)) {
        return res.status(400).send("Invalid email");
      }

      if (password != confirmPassword) throw "Wrong Password";

      if (password.length < 8) throw "Minimum 8 characters";

      const salt = await bcrypt.genSalt(10);

      const hashPass = await bcrypt.hash(password, salt);

      const data = await user.create({
        username,
        password: hashPass,
      });

      res.status(200).json({
        massage: "Register Succes",
        data,
      });
    } catch (err) {
      res.status(400).send(err);
    }
  },

  login: async (req, res) => {
    try {
      const { username, password } = req.body;

      const isUserExist = await user.findOne({
        where: {
          username: username ? username : "",
        },
        raw: true,
      });

      if (!isUserExist) throw "User not found";

      const payload = {
        username: isUserExist.username,
        isAdmin: isUserExist.isAdmin,
      };
      const token = jwt.sign(payload, "azmi");

      const isValid = await bcrypt.compare(password, isUserExist.password);

      if (!isValid) throw `Wrong password`;

      res.status(200).send({
        message: "Login Succes",
        isUserExist,
        token,
      });
    } catch (err) {
      res.status(400).send(err);
    }
  },

  findAllUser: async (req, res) => {
    try {
      let token = req.headers.authorization;

      token = token.split(" ")[1];

      let decoded = jwt.verify(token, "azmi");

      if (!decoded.isAdmin) {
        return res.status(403).json({ message: "Access forbidden" });
      }

      const users = await user.findAll({ raw: true });
      return res.status(200).send(users);
    } catch (err) {
      res.status(400).send(err);
    }
  },

  findById: async (req, res) => {
    try {
      let token = req.headers.authorization;

      token = token.split(" ")[1];

      let decoded = jwt.verify(token, "azmi");

      if (!decoded.isAdmin) {
        return res.status(403).json({ message: "Access forbidden" });
      }
      const users = await user.findOne({
        where: {
          id: req.params.id,
        },
      });
      if (!users) throw "user not found";
      res.status(200).send(users);
    } catch (err) {
      res.status(400).send(err);
    }
  },

  remove: async (req, res) => {
    try {
      let token = req.headers.authorization;

      token = token.split(" ")[1];

      let decoded = jwt.verify(token, "azmi");

      if (!decoded.isAdmin) {
        return res.status(403).json({ message: "Access forbidden" });
      }

      const userToRemove = await user.findOne({
        where: {
          id: req.params.id,
        },
      });
      if (!userToRemove) {
        res.status(404).send("User not found");
        return;
      }

      await userToRemove.destroy();
      const users = await user.findAll();
      res.status(200).send(users);
    } catch (err) {
      res.status(400).send(err);
    }
  },

  update: async (req, res) => {
    try {
      const { username, phoneNumber, gender, email, password } = req.body;
      const userToUpdate = await user.findOne({ where: { id: req.params.id } });
      if (!userToUpdate) {
        res.status(404).send("User not found");
        return;
      }

      if (password.length < 8) throw "Minimum 8 characters";

      const emailValidation = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
      if (!emailValidation.test(email)) {
        return res.status(400).send({
          message: "Invalid email",
        });
      }
      await user.update(
        {
          username,
          phoneNumber,
          gender,
          email,
          password,
        },
        {
          where: { id: req.params.id },
        }
      );
      const users = await user.findOne({ where: { id: req.params.id } });
      res.status(200).send(users);
    } catch (err) {
      res.status(400).send(err);
    }
  },
  uploadFile: async (req, res) => {
    try {
      let fileUploaded = req.file;
      await user.update(
        {
          images: fileUploaded.filename,
        },
        {
          where: {
            id: req.params.id,
          },
        }
      );
      const getImages = await user.findOne({
        where: {
          id: req.params.id,
        },
        raw: true,
      });
      res.status(200).send({
        id: getImages.id,
        images: getImages.images,
      });
    } catch (err) {
      console.log(err);
      res.status(400).send(err);
    }
  },
  changeOtp: async (req, res) => {
    try {
      const { username } = req.body;

      const code_otp = Math.floor(100000 + Math.random() * 900000).toString();

      const salt = await bcrypt.genSalt(10);
      const hashOtp = await bcrypt.hash(code_otp, salt);

      const data = await user.update(
        { code_otp: hashOtp },
        {
          where: {
            username,
          },
        }
      );

      const isAccountExist = await user.findOne({
        where: { username },
        raw: true,
      });

      const tempEmail = fs.readFileSync("./template/codeotp.html", "utf-8");
      const tempCompile = handlebars.compile(tempEmail);
      const tempResult = tempCompile({
        username: isAccountExist.username,
        code_otp,
      });

      await transporter.sendMail({
        from: "Admin",
        to: isAccountExist.email,
        subject: "Verifikasi akun",
        html: tempResult,
      });

      res.status(200).send({
        massage: "Check Your Email, code otp send succes",
        data,
      });
    } catch (err) {
      res.status(400).send(err);
    }
  },

  verification: async (req, res) => {
    try {
      const { username, code_otp } = req.body;

      const userData = await user.findOne({ where: { username } });
      if (!userData) {
        return res.status(404).send({ message: "User not found" });
      }

      const isOtpValid = await bcrypt.compare(code_otp, userData.code_otp);

      if (!isOtpValid) {
        return res.status(401).send({ message: "Invalid OTP" });
      }

      await user.update({ isVerified: true }, { where: { username } });

      res.status(200).send({
        message: "OTP verification successful",
      });
    } catch (err) {
      res.status(400).send(err);
    }
  },
  changePassword: async (req, res) => {
    try {
      const { oldPassword, newPassword, confirmPassword } = req.body;
      const salt = await bcrypt.genSalt(10);
      const hashPassword = await bcrypt.hash(newPassword, salt);

      if (newPassword !== confirmPassword)
        throw "Password doesnt match with confirm password";
      const userObj = await user.findOne({
        where: {
          username,
        },
        raw: true,
      });

      if (!userObj) throw "User Not Found";

      const isPasswordValid = await bcrypt.compare(
        oldPassword,
        userObj.password
      );

      if (!isPasswordValid) throw "Wrong Password";

      const userUpdated = await user.update(
        {
          password: hashPassword,
        },
        {
          where: { email },
        }
      );

      res.status(200).send(userObj, userUpdated);
    } catch (err) {
      res.status(400).send(err);
    }
  },
};
